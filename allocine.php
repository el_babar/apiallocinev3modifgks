<?php

class Allocine
{
    private $_api_url = 'http://api.allocine.fr/rest/v3';
    private $_partner_key;
    private $_secret_key;
    private $_user_agent = 'Dalvik/1.6.0 (Linux; U; Android 4.2.2; Nexus 4 Build/JDQ39E)';

    public function __construct($partner_key, $secret_key)
    {
        $this->_partner_key = $partner_key;
        $this->_secret_key = $secret_key;
    }

    private function _do_request($method, $params)
    {
        // build the URL
        $query_url = $this->_api_url.'/'.$method;

        // new algo to build the query
        $sed = date('Ymd');
        $sig = urlencode(base64_encode(sha1($this->_secret_key.http_build_query($params).'&sed='.$sed, true)));
        $query_url .= '?'.http_build_query($params).'&sed='.$sed.'&sig='.$sig;

        // do the request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $query_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->_user_agent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

	private function _getmyid($id,$filter)
    {
        // build the params
        $params = array(
            'partner' => $this->_partner_key,
            'code' => $id,
            'profile' => 'large',
            'filter' => $filter,
            'striptags' => 'synopsis,synopsisshort',
            'format' => 'json',
        );

        // do the request
        $response = $this->_do_request($filter, $params);

        return $response;
    }
	
	
	
	private function _get_id($query,$what) {
		    // build the params 	
			$query=str_replace("+", "", $query);	
		     $params = array(
            'partner' => $this->_partner_key,
            'q' => $query,
            'format' => 'json',
            'filter' => $what
        );

        // do the request
        $json = $this->_do_request('search', $params);
		//decode json
		$jsonarray=json_decode($json);
		//select array ($what=movie or tvseries)
		$feeds=$jsonarray->feed->$what;
		foreach($feeds as $feed) {
			//build code
			$code=$feed->code;
			//build title
			$title=$feed->originalTitle;
			//check if $query
			if(strlen($query)==strlen($title)){
				$json_get=$this->_getmyid($code,$what);	
				$fichejson=json_decode($json_get);
				
				$fichearray=$fichejson->$what;
				//prepare array
				$img=$feed->poster->href;
				$note=number_format($feed->statistics->userRating,2 ,'.', ' ');
				$urlfiche=$feed->link[0]->href;
				$synos=$fichearray->synopsis;
				$actors=$fichearray->castingShort->actors;
				$nationality=$fichearray->nationality[0]->{'$'};
				$genre=$fichearray->genre[0]->{'$'};
				
				if($what=="tvseries") {
				$creators=$fichearray->castingShort->creators;
				$namecreators="creators";
				$datestartavant=$fichearray->originalBroadcast->dateStart;
				$datestartexplode=explode("-", $datestartavant);
				$datestart=$datestartexplode[2]."/".$datestartexplode[1]."/".$datestartexplode[0];
				$productionstatus=$fichearray->productionStatus->{'$'}  ;
				$dateProd="dateStart";
				}
				if($what=="movie") {
					$creators=$fichearray->castingShort->directors;
					$namecreators="directors";
					$datestart=$fichearray->productionYear;
					$dateProd="dateProd";
					$productionstatus="" ;
				}
				
				
				$response=array(
								"code"=>$code,	
								"titre"=>$title,
								"image"=>$img,
								"note"=>$note,
								"allocineHTML"=>$urlfiche,
								"synopsis" =>$synos,
								$namecreators=>$creators,
								"actor"=>$actors,
								$dateProd=>$datestart,
								"productionStatus"=>$productionstatus,
								"nationality"=>$nationality,
								"genre"=>$genre,
								"match"=>'100'
												);
				return json_encode($response);
			}
			else {
			
				$query=str_replace(" And ", " & ", $query);	
				
				if((preg_match('/'.$query.'/', $title)) && (strlen($query)==strlen($title))){
					$json_get=$this->_getmyid($code,$what);	
					$fichejson=json_decode($json_get);
				
				$fichearray=$fichejson->$what;
				//prepare array
				$img=$feed->poster->href;
				$note=number_format($feed->statistics->userRating,2 ,'.', ' ');
				$urlfiche=$feed->link[0]->href;
				$synos=$fichearray->synopsis;
				$actors=$fichearray->castingShort->actors;
				$nationality=$fichearray->nationality[0]->{'$'};
				$genre=$fichearray->genre[0]->{'$'};
				
				if($what=="tvseries") {
				$creators=$fichearray->castingShort->creators;
				$namecreators="creators";
				$datestartavant=$fichearray->originalBroadcast->dateStart;
				$datestartexplode=explode("-", $datestartavant);
				$datestart=$datestartexplode[2]."/".$datestartexplode[1]."/".$datestartexplode[0];
				$productionstatus=$fichearray->productionStatus->{'$'}  ;
				$dateProd="dateStart";
				}
				if($what=="movie") {
					$creators=$fichearray->castingShort->directors;
					$namecreators="directors";
					$datestart=$fichearray->productionYear;
					$dateProd="dateProd";
					$productionstatus="" ;
				}
				
				
				$response=array(
								"code"=>$code,	
								"titre"=>$title,
								"image"=>$img,
								"note"=>$note,
								"allocineHTML"=>$urlfiche,
								"synopsis" =>$synos,
								$namecreators=>$creators,
								"actor"=>$actors,
								$dateProd=>$datestart,
								"productionStatus"=>$productionstatus,
								"nationality"=>$nationality,
								"genre"=>$genre,
								"match"=>'95'
												);
				return json_encode($response);
				}/**
				else {
					if(preg_match('/'.$query.'/', $title)){
						$json_get=$this->_getmyid($code,$what);	
					$fichejson=json_decode($json_get);
				
				$fichearray=$fichejson->$what;
				//prepare array
				$img=$feed->poster->href;
				$note=number_format($feed->statistics->userRating,2 ,'.', ' ');
				$urlfiche=$feed->link[0]->href;
				$synos=$fichearray->synopsis;
				$actors=$fichearray->castingShort->actors;
				$nationality=$fichearray->nationality[0]->{'$'};
				$genre=$fichearray->genre[0]->{'$'};
				
				if($what=="tvseries") {
				$creators=$fichearray->castingShort->creators;
				$namecreators="creators";
				$datestartavant=$fichearray->originalBroadcast->dateStart;
				$datestartexplode=explode("-", $datestartavant);
				$datestart=$datestartexplode[2]."/".$datestartexplode[1]."/".$datestartexplode[0];
				$productionstatus=$fichearray->productionStatus->{'$'}  ;
				$dateProd="dateStart";
				}
				if($what=="movie") {
					$creators=$fichearray->castingShort->directors;
					$namecreators="directors";
					$datestart=$fichearray->productionYear;
					$dateProd="dateProd";
					$productionstatus="" ;
				}
				
				
				$response=array(
								"code"=>$code,	
								"titre"=>$title,
								"image"=>$img,
								"note"=>$note,
								"allocineHTML"=>$urlfiche,
								"synopsis" =>$synos,
								$namecreators=>$creators,
								"actor"=>$actors,
								$dateProd=>$datestart,
								"productionStatus"=>$productionstatus,
								"nationality"=>$nationality,
								"genre"=>$genre,
								"match"=>'92'
												);
				return $json;
					}*/
					else {
						similar_text($query, $title,$simtitle);
						if($simtitle >="90") {
							$json_get=$this->_getmyid($code,$what);	
					$fichejson=json_decode($json_get);
				
				$fichearray=$fichejson->$what;
				//prepare array
				$img=$feed->poster->href;
				$note=number_format($feed->statistics->userRating,2 ,'.', ' ');
				$urlfiche=$feed->link[0]->href;
				$synos=$fichearray->synopsis;
				$actors=$fichearray->castingShort->actors;
				$nationality=$fichearray->nationality[0]->{'$'};
				$genre=$fichearray->genre[0]->{'$'};
				
				if($what=="tvseries") {
				$creators=$fichearray->castingShort->creators;
				$namecreators="creators";
				$datestartavant=$fichearray->originalBroadcast->dateStart;
				$datestartexplode=explode("-", $datestartavant);
				$datestart=$datestartexplode[2]."/".$datestartexplode[1]."/".$datestartexplode[0];
				$productionstatus=$fichearray->productionStatus->{'$'}  ;
				$dateProd="dateStart";
				}
				if($what=="movie") {
					$creators=$fichearray->castingShort->directors;
					$namecreators="directors";
					$datestart=$fichearray->productionYear;
					$dateProd="dateProd";
					$productionstatus="" ;
				}
				
				
				$response=array(
								"code"=>$code,	
								"titre"=>$title,
								"image"=>$img,
								"note"=>$note,
								"allocineHTML"=>$urlfiche,
								"synopsis" =>$synos,
								$namecreators=>$creators,
								"actor"=>$actors,
								$dateProd=>$datestart,
								"productionStatus"=>$productionstatus,
								"nationality"=>$nationality,
								"genre"=>$genre,
								"match"=>'90'
												);
				return json_encode($response);
						}
						else {
							$scdtitle=$feed->title;
							if((preg_match('/'.$query.'/', $scdtitle)) && (strlen($query)===strlen($scdtitle))){
							$json_get=$this->_getmyid($code,$what);	
							$fichejson=json_decode($json_get);
				
							$fichearray=$fichejson->$what;
							//prepare array
							$img=$feed->poster->href;
				$note=number_format($feed->statistics->userRating,2 ,'.', ' ');
				$urlfiche=$feed->link[0]->href;
				$synos=$fichearray->synopsis;
				$actors=$fichearray->castingShort->actors;
				$nationality=$fichearray->nationality[0]->{'$'};
				$genre=$fichearray->genre[0]->{'$'};
				
				if($what=="tvseries") {
				$creators=$fichearray->castingShort->creators;
				$namecreators="creators";
				$datestartavant=$fichearray->originalBroadcast->dateStart;
				$datestartexplode=explode("-", $datestartavant);
				$datestart=$datestartexplode[2]."/".$datestartexplode[1]."/".$datestartexplode[0];
				$productionstatus=$fichearray->productionStatus->{'$'}  ;
				$dateProd="dateStart";
				}
				if($what=="movie") {
					$creators=$fichearray->castingShort->directors;
					$namecreators="directors";
					$datestart=$fichearray->productionYear;
					$dateProd="dateProd";
					$productionstatus="" ;
				}
				
				
				$response=array(
								"code"=>$code,	
								"titre"=>$title,
								"image"=>$img,
								"note"=>$note,
								"allocineHTML"=>$urlfiche,
								"synopsis" =>$synos,
								$namecreators=>$creators,
								"actor"=>$actors,
								$dateProd=>$datestart,
								"productionStatus"=>$productionstatus,
								"nationality"=>$nationality,
								"genre"=>$genre,
								"match"=>'85'
												);
				return json_encode($response);
							}
							
						}
					}	
				//}	
			}
		} 
		
		
		
		
		
		
		
		
		
	}
	
	
    public function search($query,$what)
    {
        
     

        // do the request
        $response = $this->_get_id($query, $what);

        return $response;
    }
	
	public function gksPrez($query,$what){
		$json=json_decode($this->search($query, $what));
		if($what=="tvseries") {
			$dateProd="dateStart";
			$namecreators="creators";
			
		}
		else {
			$namecreators="directors";
					
			$dateProd="dateProd";
			
		}
		$response = 
"<pre>[center][color=red][size=7][b]".$json->titre."[/b][/size][/color]

[size=4][img]".$json->image."[/img]

[img]https://s.gks.gs/img/prez/separate/Gks/syn_film.png[/img]

[i]".$json->synopsis."[/i]


[/size][size=5][b]".$json->note."/5[/b][/size][size=4]

[img]https://s.gks.gs/img/prez/separate/Gks/info_film.png[/img]

[b][u]Genre[/u] : [/b] [i]".$json->genre."[/i]
[b][u]Origine[/u] : [/b] [i]".$json->nationality."[/i]
[b][u]Réalisateur[/u] : [/b] [i]".$json->$namecreators."[/i]
[b][u]Acteurs[/u] : [/b] [i]".$json->actor."[/i]
[b][u]Titre Original[/u] : [/b] [i]".$json->titre."[/i]
[b][u]Année de production[/u] : [/b] [i]".$json->$dateProd."[/i]
[b][u]Lien vers Allociné[/u] : [/b] [i]".$json->allocineHTML."[/i]

[img]https://s.gks.gs/img/prez/separate/Gks/up_serie.png[/img]

[b][u]Qualité[/u]: [/b] [i]....................................[/i]
[b][u]Format[/u]: [/b] [i]....................................[/i]
[b][u]Langue[/u]: [/b] [i]....................................[/i]
[b][u]Sous-titres[/u]: [/b] [i]....................................[/i]

[/size]
[img]https://s.gks.gs/img/prez/separate/Gks/restez_en_seed.png[/img][/center]</pre>";
return $response;
		
	}
    
	
	
	
	
	
}